﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

public class ActionStream<T> : IActionStream<T>, IDisposable
{
    private DisposableConnectionsList<T> _disposableActions;

    public ActionStream()
    {
        _disposableActions = new DisposableConnectionsList<T>();
    }

    public IDisposable Listen(Action<T> act)
    {
        if (_disposableActions == null)
            throw new ObjectDisposedException("Trying to access disposed stream");
        return _disposableActions.Add(act);
    }

    public IDisposable Listen(Action act)
    {
        return Listen(targ => act());
    }

    public void Send(T value)
    {
        if (_disposableActions == null)
            return;
        if (_disposableActions.Count < 1)
            return;
        _disposableActions.React(value);
    }

    public void Dispose()
    {
        _disposableActions.Clear();
        _disposableActions = null;
    }
}

public class ActionStream : ActionStream<Point>
{
    public void Send()
    {
        Send(new Point());
    }
}

public class DisposableConnectionsList<T>
{
    private readonly List<Action<T>> _disposableActions;

    public DisposableConnectionsList()
    {
        _disposableActions = new List<Action<T>>();
    }

    public IDisposable Add(Action<T> item)
    {
        _disposableActions.Add(item);
        return new AnonymousDisposable(() => _disposableActions.Remove(item));
    }    

    public void Clear()
    {
        _disposableActions.Clear();
    }

    public void React(T value)
    {
        _disposableActions.ForEach(action => action(value));
    }

    public int Count
    {
        get { return _disposableActions.Count; }
    }
}

class AnonymousDisposable : IDisposable
{
    private bool _isDisposed = false;
    private readonly Action _dispose;

    public AnonymousDisposable(Action dispose)
    {
        _dispose = dispose;
    }

    public void Dispose()
    {
        if (!_isDisposed)
        {
            _isDisposed = true;
            _dispose();
        }
    }
}

public interface IActionStream
{
    IDisposable Listen(Action act);
}

public interface IActionStream<T> : IActionStream
{
    IDisposable Listen(Action<T> act);
}

public interface IVariableListener
{
    IDisposable OnChange(Action action);
}

public interface IVariableListener<T> : IVariableListener
{
    IDisposable OnChange(Action<T> action);

    IDisposable OnChangeInstant(Action<T> action);
}

[Serializable]
public class ReactiveHolder<T> : IVariableListener<T>
{
    [JsonProperty] private T _value;

    [JsonIgnore]
    public T Value
    {
        get { return _value; }
        set
        {
            _value = value;
            _stream.Send(_value);
        }
    }

    public ReactiveHolder()
    {
        _stream = new ActionStream<T>();
    }

    private ActionStream<T> _stream;

    public IDisposable OnChange(Action action)
    {
        return _stream.Listen(action);
    }

    public IDisposable OnChange(Action<T> action)
    {
        return _stream.Listen(action);
    }

    public IDisposable OnChangeInstant(Action<T> action)
    {
        action(_value);
        return _stream.Listen(action);
    }
}

public class ConnectionCollector : List<IDisposable>, IDisposable
{
    public IDisposable add { set { Add(value); } }

    public void DisconnectAll()
    {
        foreach(var c in this)
        {
            c.Dispose();
        }

        Clear();
    }

    public void Dispose() { DisconnectAll(); }
    public void Collect(IDisposable connection)
    {
        add = connection;
    }
}
