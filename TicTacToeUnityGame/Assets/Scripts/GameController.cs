﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController
{
    private GameModel _model;
    private PlayerController[] _playerControllers;

    private bool _isFistTurn = true;

    public ActionStream<string> ChangeTurnStream = new ActionStream<string>();

    public void InitGame(GameModel model, PlayerController[] playerControllers, int forcePlayerId = -1)
    {
        _model = model;
        if (_model.CommandsHistory == null)
            _model.CommandsHistory = new List<TicTacCommand>();
        _playerControllers = playerControllers;
        _model.CurrentPlayerId = forcePlayerId == -1 ? Random.Range(0, _playerControllers.Length) : forcePlayerId;
        if (_model.CurrentPlayerId > _playerControllers.Length)
            _model.CurrentPlayerId = 0;
        _model.PlayersInGameUids =
            _playerControllers.Select(playerController => playerController.GetPlayerUid()).ToArray();
        ChangeTurn();
    }

    public bool SetPoint(Point pointData, string playerUid)
    {
        if (_model.MapState[pointData.Y, pointData.X] == -1 && GetCurrentPlayer().GetPlayerUid() == playerUid &&
            !_model.GameEnd)
        {
            _model.MapState[pointData.Y, pointData.X] = _model.CurrentPlayerId;
            _model.MapStateChangeStream.Send(pointData);
            var result = CheckEndGame();
            if (result == -1)
            {
                ChangeTurn();
            }
            else
            {
                EndGame(result);
            }

            return true;
        }

        return false;
    }

    public void ChangeTurn()
    {
        if (!_isFistTurn)
        {
            _model.CurrentPlayerId++;
            if (_model.CurrentPlayerId >= _playerControllers.Length)
                _model.CurrentPlayerId = 0;
        }
        else
        {
            _isFistTurn = false;
        }

        var currentPlayer = GetCurrentPlayer();
        var uid = currentPlayer.GetPlayerUid();
        if (_model.GameType == GameTypeEnum.DevicePvP)
            _model.DevicePlayerUid = uid;
        var devicePlayerTurn = uid == _model.DevicePlayerUid;
        if (devicePlayerTurn)
        {
            InputController.Instance.CurrentPlayer = currentPlayer;
        }

        ChangeTurnStream.Send(uid);
    }

    public int CheckEndGame()
    {
        int winner = CheckLine(true);
        if (winner == -1)
            winner = CheckLine(false);
        if (winner == -1)
            winner = CheckDiagonale(true);
        if (winner == -1)
            winner = CheckDiagonale(false);
        if (winner == -1)
            winner = !AnyTurnLeft() ? -2 : -1;
        return winner;
    }

    public int CheckDiagonale(bool first)
    {
        int winner = -1;
        var firstLength = _model.MapState.GetLength(0);
        for (int i = 0; i < firstLength; i++)
        {
            int j = first ? i : firstLength - (i + 1);
            var mapNode = _model.MapState[i, j];
            if (mapNode == -1)
            {
                winner = -1;
                break;
            }

            if (winner == -1)
            {
                winner = mapNode;
            }
            else
            {
                if (winner != mapNode)
                {
                    winner = -1;
                    break;
                }
            }
        }

        return winner;
    }

    public int CheckLine(bool horizontal)
    {
        int winner = -1;
        var firstLength = _model.MapState.GetLength(horizontal ? 1 : 0);
        var secondLength = _model.MapState.GetLength(horizontal ? 0 : 1);
        for (int i = 0; i < firstLength; i++)
        {
            for (int j = 0; j < secondLength; j++)
            {
                var mapNode = horizontal ? _model.MapState[i, j] : _model.MapState[j, i];
                if (mapNode == -1)
                {
                    winner = -1;
                    break;
                }

                if (winner == -1)
                {
                    winner = mapNode;
                }
                else
                {
                    if (winner != mapNode)
                    {
                        winner = -1;
                        break;
                    }
                }
            }

            if (winner != -1)
                return winner;
        }

        return -1;
    }

    public bool AnyTurnLeft()
    {
        var firstLength = _model.MapState.GetLength(0);
        var secondLength = _model.MapState.GetLength(1);
        for (int i = 0; i < firstLength; i++)
        {
            for (int j = 0; j < secondLength; j++)
            {
                var mapNode = _model.MapState[i, j];
                if (mapNode == -1)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void EndGame(int winner)
    {
        if (winner == -2)
            Debug.Log("Draw");
        Debug.Log("Winner id is " + winner);
        _model.GameEnd = true;
    }

    public PlayerController GetCurrentPlayer()
    {
        return _playerControllers[_model.CurrentPlayerId];
    }

    public void AcceptCommand(TicTacCommand command)
    {
        if (command.CanExecute(this))
        {
            command.Execute(this);
            _model.CommandsHistory.Add(command);
        }
    }
}