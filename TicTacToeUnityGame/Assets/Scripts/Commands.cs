﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TicTacCommand
{
    public virtual void Execute(GameController gameController)
    {
    }
    public virtual bool CanExecute(GameController gameController)
    {
        return true;
    }
}

public class SetPointCommand : TicTacCommand
{
    public string PlayerUid;
    public Point TargetPoint;
    public override void Execute(GameController gameController)
    {
        gameController.SetPoint(TargetPoint, PlayerUid);
    }

    public override bool CanExecute(GameController gameController)
    {
        return gameController.GetCurrentPlayer().GetPlayerUid() == PlayerUid;
    }
}