﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractFactoryUI
{
    private static AbstractFactoryUI _instance;
    
    public static AbstractFactoryUI Instance
    {
        get { return _instance; }
        set { _instance = value; }
    }

    public virtual RectTransform CreateBackground()
    {
        return null;
    }

    public virtual Button CreateButton()
    {
        return null;
    }

    public virtual GridLayoutGroup CreateGamePanel()
    {
        return null;
    }

    public virtual NodeView CreateGameNode()
    {
        return null;
    }
}

public class UnityFactoryUI : AbstractFactoryUI
{
    private RectTransform _cashedBackground = null;

    public override RectTransform CreateBackground()
    {
        if (_cashedBackground == null)
            _cashedBackground = Resources.Load<RectTransform>("Prefabs/SimpleBackground");
        return Object.Instantiate(_cashedBackground);
    }

    private GridLayoutGroup _cashedGamePanel = null;

    public override GridLayoutGroup CreateGamePanel()
    {
        if (_cashedGamePanel == null)
            _cashedGamePanel = Resources.Load<GridLayoutGroup>("Prefabs/GameGrid");
        return Object.Instantiate(_cashedGamePanel);
    }

    private NodeView _cashedNodeView = null;

    public override NodeView CreateGameNode()
    {
        if (_cashedNodeView == null)
            _cashedNodeView = Resources.Load<NodeView>("Prefabs/NodeButton");
        return Object.Instantiate(_cashedNodeView);
    }
}
