﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class GameModel
{
    public int[,] MapState;
    public int CurrentPlayerId;
    public string[] PlayersInGameUids;

    public string DevicePlayerUid;

    public bool GameEnd = false;

    public List<TicTacCommand> CommandsHistory;
    
    public GameTypeEnum GameType;
    [JsonIgnore] public ActionStream<Point> MapStateChangeStream = new ActionStream<Point>();
}