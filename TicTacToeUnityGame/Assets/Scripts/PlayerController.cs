﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController
{
    private PlayerModel _model;
    private GameController _gameController;

    public void InitPlayer(PlayerModel model, GameController gameController)
    {
        _model = model;
        _gameController = gameController;
        _gameController.ChangeTurnStream.Listen(uid => ChangeTurn(uid == GetPlayerUid()));
    }

    public void SetMapPoint(Point pointData)
    {
        _gameController.AcceptCommand(new SetPointCommand() {TargetPoint = pointData, PlayerUid = GetPlayerUid()});
    }

    public void ChangeTurn(bool myTurn)
    {
        if (myTurn)
        {
            
        }
    }

    public string GetPlayerUid()
    {
        return _model.Uid;
    }
}
