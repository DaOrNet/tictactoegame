﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController
{
    private static InputController _instance;

    public static InputController Instance
    {
        get { return _instance = _instance ?? new InputController(); }
    }

    public PlayerController CurrentPlayer;
    
    public void HandleInput(int yPos, int xPos)
    {
        CurrentPlayer.SetMapPoint(new Point(xPos,yPos));
    }
}