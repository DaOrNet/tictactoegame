﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameView : ModifiedBehaviour
{
    public RectTransform FullGameRect;

    public GameInitializer Initializer;

    public NodeView[,] GameNodes;

    public ConnectionCollector Collector = new ConnectionCollector();
    
    private void Awake()
    {
        Initializer = new GameInitializer();
        Initializer.InitializeGame();
        AbstractFactoryUI.Instance = new UnityFactoryUI();
    }

    void Start()
    {
        var UiFactory = AbstractFactoryUI.Instance;
        var background = UiFactory.CreateBackground();
        background.SetParent(FullGameRect);
        background.SetActiveSafe(true);
        background.NormalizeSize();
        background.SetFullAnchorsZone();
        background.SetFillFullZone();
      
        var gamePanel = UiFactory.CreateGamePanel();
        gamePanel.transform.SetParent(background);
        gamePanel.SetActiveSafe(true);
        gamePanel.transform.NormalizeSize();
        gamePanel.transform.SetToCenter();
        
        
        var gameState = Initializer.GameState;
        var map = gameState.MapState;
        GameNodes = new NodeView[map.GetLength(0), map.GetLength(1)];
        for (int i = 0; i < map.LongLength; i++)
        { 
            var mapNode = UiFactory.CreateGameNode();
            mapNode.transform.SetParent(gamePanel.transform);
            mapNode.transform.NormalizeSize();
            var yPos = i / map.GetLength(0);
            var xPos = i - yPos * map.GetLength(0);
            Collector.add = mapNode.TargetButton.ClickStream().Listen(() => { InputControllerInstance.HandleInput(yPos, xPos); });
            GameNodes[yPos, xPos] = mapNode;
        }

        Collector.add = gameState.MapStateChangeStream.Listen(point =>
        {
            GameNodes[point.Y, point.X].CurrentText.text = GetTextForPlayer(gameState.CurrentPlayerId);
        });
    }

    public string GetTextForPlayer(int playerId)
    {
        switch (playerId)
        {
            case 0: return "X";
            case 1: return "O";
        }

        return "{";
    }
}