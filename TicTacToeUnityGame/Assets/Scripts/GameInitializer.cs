﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;

public class GameInitializer
{
    public GameModel GameState;
    public GameController CurrentGameController;
    public PlayerModel[] PlayerModels;
    public PlayerController[] PlayerControllers;

    public void InitializeGame()
    {
        GameState = new GameModel()
        {
            MapState = new int[3, 3]
            {
                {-1, -1, -1},
                {-1, -1, -1},
                {-1, -1, -1},
            },
            PlayersInGameUids = new string[2],
            CurrentPlayerId = 0,
            GameType = GameTypeEnum.DevicePvP,
        };
        CurrentGameController = new GameController();

        PlayerModels = new PlayerModel[2];
        PlayerModels[0] = new PlayerModel()
        {
            Uid = Guid.NewGuid().ToString(),
        };
        PlayerModels[1] = new PlayerModel()
        {
            Uid = Guid.NewGuid().ToString(),
        };
        PlayerControllers = new PlayerController[2];

        PlayerControllers[0] = new PlayerController();
        PlayerControllers[0].InitPlayer(PlayerModels[0], CurrentGameController);

        PlayerControllers[1] = new PlayerController();
        PlayerControllers[1].InitPlayer(PlayerModels[1], CurrentGameController);

        GameState.DevicePlayerUid = PlayerModels[0].Uid;

        CurrentGameController.InitGame(GameState, PlayerControllers, 0);
    }
}