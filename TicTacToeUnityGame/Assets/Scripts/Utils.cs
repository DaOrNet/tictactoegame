﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Utils
{
    public static IActionStream ClickStream(this Button button)
    {
        var actionStream = new ActionStream();
        button.onClick.AddListener(() => { actionStream.Send(); });
        return actionStream;
    }

    public static void ForEach<T>(this IEnumerable<T> array, Action<T> act)
    {
        foreach (var variable in array)
        {
            act(variable);
        }   
    }

    public static void SetFillFullZone(this RectTransform rect)
    {
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);
    }
    
    public static void SetFullAnchorsZone(this RectTransform rect)
    {
        rect.anchorMin = new Vector2(0, 0);
        rect.anchorMax = new Vector2(1, 1);
    }
    
    public static void NormalizeSize(this Transform rect)
    {
        rect.localScale = Vector2.one;
    }
    
    public static void SetToCenter(this Transform rect)
    {
        rect.localPosition = Vector3.one;
    }

    public static void SetActiveSafe(this Component component, bool state)
    {
        SetActiveSafe(component.gameObject, state);
    }

    public static void SetActiveSafe(this GameObject gameObject, bool state)
    {
        if (gameObject == null) return;
        if (gameObject.activeSelf == state) return;
        gameObject.SetActive(state);
    }
}

public struct Point
{
    public int X;
    public int Y;

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }
}

public enum GameTypeEnum
{
    DevicePvP,
    DevicePvE,
    ServerPvP
}